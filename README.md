![](docs/images/Bitpanda_logo.jpg)

# Getting Started

To run the project, open `code/Bitpanda.xcworkspace/` with Xcode 13.2.1 and hit run.

At first, you should see an error screen, this is intentional. Please tap the retry button to see the actual screens with data.

# Main points

* More than 80% unit test coverage.
* Extra features: rotation and dark mode support.
* No class has more than 100 LOC.
* No method is larger than 10 LOC.

# Content Table

[[_TOC_]]

# Screenshots


| Light mode | Dark mode | Larger text |
|------------|-----------|-------------|
| ![](docs/images/Bitpanda_assets_light.png) | ![](docs/images/Bitpanda_assets_dark.png) | ![](docs/images/Bitpanda_assets_large.png) | 


# Controversial Decisions

I made some decisions in this project that can seem unconventional or unpopular. I don't like being dogmatic, there is no silver bullet in software development, so I try to avoid assumptions and make every decision based on pros and cons _for a given context_, trying to figure out what is the best approach for each specific situation.

## Architecture 

This project uses the MVC architectural pattern, which has earned a reputation a generating huge view controllers that are hard to test and maintain. However, if the developer focuses on applying the SOLID principles, that can be avoided in several ways, for example:
* the subviews can became new classes, 
* the view controller can be decomposed in smaller child view controller 
* models and services can be helpful transforming the data
* datasources and delegates can be separate classes.

*Pros:*

* Simple to implement and to understand.
* If you are using the iOS SDK, it is already there, you cannot escape from it (unless you use SwiftUI)

*Cons:*

* It requires more attention to keeps concerns separated.

> IMPORTANT NOTE: All architectures are great, they solve different problems and have a different set of pros and cons. This is just my analysis for this particular app, I'm *not* saying that other architectures are inherently bad or that they should never be used.

I have considered using MVP, MVVM and VIPER for this project. Even though these are great architectures, there is a context issue: the app has only three screens. There was no need for me to use a more complex architecture with features I would not need. If having a view-model comes in handy for a specific feature, using MVC doesn't stop me from using one. But if I don't need a view model for 90% of the features, then using MVP forces me to have a presenter with its assocciated boilerplate.

## UI with Storyboard

Storyboards can lead to having a very big file that its hard to maintain. Very much like MVC leading to having huge view controllers: it is true but is also up to the engineer designing the implementation.
If the project has a lot of screens, they can be implemented through different storyboards.

*Pros:*
* Drag&Drop is faster than code. Both for creation and reading.
* Table cell prototypes in the same place, no need for a separate xib, no need to register by code.
* Navigation through segues: fewer code, visual representation of the navigation.

*Cons:*
* prepareForSegue method relies in string literals. 
* prepareForSegue becomes the only method to setup all transitions, which can lead to a long method. There are ways to overcome this problem, such as implementing a Chain Of Responsability pattern. Also, the screens in this project do not have many segues.

## Custom Parsing

The classes in the model layer are simpler than their JSON representation. That meant I had to write a custom implementation for the init(from:Decoder)
I made that decision thinking about the ease of use of the models in the other layers. 

![Model layer](diagrams/Bitpanda_models.png)

The JSON structure probably has a reason but I couldn't find any for this particular app.
Also, I thought it would be nice to show some custom parsing.

## Completion Block Instead Of async/await

I would definitely use async/await instead of completion blocks for the service layer. It has a clearer, shorter and less complex syntax. 
It was just a matter of time -- I have more experience with closures and I knew it would help me delivery the product on time.

## Pods folder

This project uses Cocoapods but I decided to add the Pods folder to the repo. That means that you can run `pod install` as well as other commands, but it is not necessary to run the project.
I am well aware that ignoring the Pods folder is the most common practice (and I acknowledge there's good reason for that). But even the [Cocoapods documentation](https://guides.cocoapods.org/using/using-cocoapods.html#should-i-check-the-pods-directory-into-source-control) makes good points for and against this. I made this decision based on:
* *Integrity:* it is very important for me that this project runs without problems. If I want to make sure that it a possible to recreate a given version, then it is a wise choice to depend only on the one repository that I have under my control, and _not to_ depend on external tools to deliver the project.
* *Few pods*: this project uses just one pod, so storage space is not (and will not be) an issue.

## Design 

The general design follows a typical MVCS pattern.

![General design](diagrams/Bitpanda_general.png)

This section explains the most unconvential parts of the app design.

### Overlay Controller

The OverlayController handles the loading and error states. 
Its main advantage is that it can be fully set up from the storyboard. The view controller just needs to make `hide()` and `show()` calls. 
The following object diagram shows the OverlayControllers used in the AssetViewController.

![Overlay Controller Object Diagram](diagrams/Bitpanda_overlayController.png)

### Assets filtering

`ArrayDataSource` is an implementation of UITableDataSource, that lets you display an homogeneous list of cells and provides a clousure property to configure each cell.
The AssetsViewController uses a `FilteredDataSourceProvider` to retrieve the right instance of the datasource depending on the selected asset type.
On runtime, the data source provider is actually `AssetsTableDataSource`, that holds a reference to three `ArrayDataSource`s, one for each type of asset.
When the "All" option is selected, the AssetsTableDataSource doesn't create the cells by itself, but it rathers fowards the calls to each of the `ArrayDataSource`s.

![Assets datasource](diagrams/Bitpanda_filtering.png)

## Tools

I used several tools for the development process:
* 👩🏽‍💻 Xcode 13.2.1 as the IDE.
* 🍫 Cocoapods for dependency management.
* 🖼 SVGKit to render the assets logos.
* ❴❵ SwiftFormat for code formatting.
* 📈 VioletUML for the UML diagrams.
* 🔢 Apple Numbers to keep track of requirements.
* 📝 Apple Pages for documentation.


