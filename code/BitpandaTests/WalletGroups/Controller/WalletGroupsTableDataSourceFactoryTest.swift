//
//  WalletGroupsTableDataSourceFactoryTest.swift
//  BitpandaTests
//
//  Created by Juan Andres Gebhard on 29/04/2022.
//

@testable import Bitpanda
import XCTest

class WalletGroupsTableDataSourceFactoryTest: XCTestCase {
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    func testSetup_WithWalletGroup_SetupsCell() {
        let dataSource = WalletGroupsTableDataSourceFactory().dataSource(wallets: Wallets())
        let walletGroup = WalletGroup(name: "Group 1", icon: "pencil", wallets: [])
        let cell = makeCell()

        dataSource.setup(cell, walletGroup, .light)

        XCTAssertEqual(walletGroup.name, cell.titleLabel.text)
        XCTAssertEqual("0 wallets", cell.subtitleLabel.text)
        XCTAssertNotNil(cell.iconImageView.image)
    }

    func makeCell() -> WalletGroupTableViewCell {
        let cell = WalletGroupTableViewCell()
        cell.titleLabel = UILabel()
        cell.subtitleLabel = UILabel()
        cell.iconImageView = UIImageView()
        return cell
    }
}
