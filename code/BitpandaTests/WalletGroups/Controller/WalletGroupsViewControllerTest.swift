//
//  WalletGroupsViewControllerTest.swift
//  BitpandaTests
//
//  Created by Juan Andres Gebhard on 28/04/2022.
//

@testable import Bitpanda
import XCTest

class WalletGroupsViewControllerTest: XCTestCase {
    var controller: WalletGroupsViewController!
    var walletsController: WalletsViewController!
    override func setUpWithError() throws {
        let board = UIStoryboard(name: "Main", bundle: nil)
        controller = (board.instantiateViewController(withIdentifier: "WalletsGroups") as! WalletGroupsViewController)
        walletsController = (board.instantiateViewController(withIdentifier: "Wallets") as! WalletsViewController)
    }

    func testInit_InitialStateIsLoading() {
        XCTAssertEqual(.loading, controller.state)
    }

    func testViewDidLoad_TableDataSourceIsNil() {
        _ = controller.view

        XCTAssertNil(controller.tableView?.dataSource)
    }

    func testSetState_WithData_SetupsDataSource() throws {
        _ = controller.view
        let data = try TestMasterdata.data()

        controller.state = .data(data)

        XCTAssertNotNil(controller.tableView?.dataSource)
        XCTAssert(controller.dataSource === controller.tableView?.dataSource)
    }

    func testPrepareForSegue_WithDataAndSelectedRow_SetupsDestinationViewController() throws {
        let segue = UIStoryboardSegue(identifier: "", source: controller, destination: walletsController)
        let data = try TestMasterdata.data()
        _ = controller.view
        controller.state = .data(data)
        controller.tableView?.selectRow(at: IndexPath(row: 0, section: 0), animated: false, scrollPosition: .none)

        controller.prepare(for: segue, sender: nil)

        XCTAssertEqual(data.assets, walletsController.assets)
        XCTAssertEqual("Cryptocoins", walletsController.navigationItem.title)
    }

    func testPrepareForSegue_WithoutSelectedRow_DoesNotSetupDestinationViewController() throws {
        let segue = UIStoryboardSegue(identifier: "", source: controller, destination: walletsController)
        let data = try TestMasterdata.data()
        _ = controller.view
        controller.state = .data(data)

        controller.prepare(for: segue, sender: nil)

        XCTAssertEqual(Assets(), walletsController.assets)
        XCTAssertNil(walletsController.navigationItem.title)
    }

    func testSetState_WithError_AddsTheErrorControllerOverlay() {
        _ = controller.view

        controller.state = .error(ServiceError.resourceNotFound) {}

        if let overlay = controller.errorController.overlayView {
            XCTAssertEqual(overlay.superview, controller.view)
        }
    }
}
