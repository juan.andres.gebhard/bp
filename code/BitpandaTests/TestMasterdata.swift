//
//  TestMasterdata.swift
//  BitpandaTests
//
//  Created by Juan Andres Gebhard on 29/04/2022.
//

@testable import Bitpanda
import Foundation

class TestMasterdata {
    class func data() throws -> MasterData {
        let parser = Parser<MasterData>()
        let url = Bundle.main.url(forResource: "Masterdata", withExtension: "json")
        let data = try Data(contentsOf: url!)
        let result = parser.parse(data: data)
        if case let .success(assets) = result {
            return assets
        }
        throw ServiceError.resourceNotFound
    }
}
