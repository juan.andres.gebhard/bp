//
//  LocalFileServiceTests.swift
//  BitpandaTests
//
//  Created by Juan Andres Gebhard on 23/04/2022.
//

@testable import Bitpanda
import XCTest

class LocalFileServiceTests: XCTestCase {
    var service: LocalFileService!

    override func setUpWithError() throws {
        service = LocalFileService()
        service.delay = 0
    }

    override func tearDownWithError() throws {}

    func testFetchData_FailsTheFirstTime() throws {
        let expectation = XCTestExpectation(description: "Parse is successfull")

        service.fetchAssets { (result: Result<MasterData, Error>) in
            if case .failure = result {
                expectation.fulfill()
            }
        }
        wait(for: [expectation], timeout: 0.1)
    }

    func testFetchData_SucceedsTheSecondTime() throws {
        let expectation = XCTestExpectation(description: "Parse is successfull")

        service.fetchAssets { (_: Result<MasterData, Error>) in
            self.service.fetchAssets { (secondResult: Result<MasterData, Error>) in
                if case .success = secondResult {
                    expectation.fulfill()
                }
            }
        }
        wait(for: [expectation], timeout: 0.1)
    }
}
