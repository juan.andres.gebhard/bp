//
//  AssetsViewControllerTest.swift
//  BitpandaTests
//
//  Created by Juan Andres Gebhard on 26/04/2022.
//

@testable import Bitpanda
import XCTest

class AssetsServiceMock: MasterDataService {
    var wasCalled = false
    var data: MasterData = .init()

    func fetchAssets(completion: @escaping (Result<MasterData, Error>) -> Void) {
        wasCalled = true
        completion(.success(data))
    }
}

class AssetsServiceFailMock: MasterDataService {
    var error = ServiceError.resourceNotFound
    var callsCount = 0
    func fetchAssets(completion: @escaping (Result<MasterData, Error>) -> Void) {
        callsCount += 1
        completion(.failure(error))
    }
}

class DataSourceProviderMock: FilteredDataSourceProvider {
    var selectedFilter: AssetType!
    func filteredDataSource(filter: AssetType) -> UITableViewDataSource {
        selectedFilter = filter
        return AssetsTableDataSource()
    }
}

class AssetsViewControllerTest: XCTestCase {
    var controller: AssetsViewController!

    override func setUpWithError() throws {
        let board = UIStoryboard(name: "Main", bundle: nil)
        controller = (board.instantiateViewController(withIdentifier: "Assets") as! AssetsViewController)
    }

    func testInit_StartsWithLoadingState() {
        XCTAssertEqual(State.loading, controller.state)
    }

    func testViewDidLoad_CallsAssetsService() {
        let service = AssetsServiceMock()
        controller.service = service

        _ = controller.view

        XCTAssert(service.wasCalled)
    }

    func testViewDidLoad_ServiceSucceeds_TransitionsToDataState() {
        let service = AssetsServiceMock()
        controller.service = service

        _ = controller.view

        XCTAssertEqual(.data(service.data), controller.state)
    }

    func testViewDidLoad_SeviceFails_TransitionsToErrorState() {
        let service = AssetsServiceFailMock()
        controller.service = service

        _ = controller.view

        XCTAssertEqual(.error(service.error) {}, controller.state)
    }

    func testViewDidLoad_SeviceFails_RetryCallbackCallService() {
        let service = AssetsServiceFailMock()
        controller.service = service

        _ = controller.view

        if case let .error(_, retryBlock) = controller.state {
            retryBlock()
        }
        XCTAssertEqual(2, service.callsCount)
    }

    func testFilterAction_FirstItemSelected_FiltersCryptocoin() {
        let provider = DataSourceProviderMock()
        controller.dataSourceProvider = provider
        _ = controller.view
        controller.filterControl.selectedSegmentIndex = 0

        controller.filterAction(self)

        XCTAssertEqual(.cryptocoins, provider.selectedFilter)
    }

    func testFilterAction_SecondItemSelected_FiltersCommodities() {
        let provider = DataSourceProviderMock()
        controller.dataSourceProvider = provider
        _ = controller.view
        controller.filterControl.selectedSegmentIndex = 1

        controller.filterAction(self)

        XCTAssertEqual(.commodities, provider.selectedFilter)
    }

    func testFilterAction_ThirdItemSelected_FiltersFiats() {
        let provider = DataSourceProviderMock()
        controller.dataSourceProvider = provider
        _ = controller.view
        controller.filterControl.selectedSegmentIndex = 2

        controller.filterAction(self)

        XCTAssertEqual(.fiat, provider.selectedFilter)
    }

    func testFilterAction_FourthItemSelected_FiltersAll() {
        let provider = DataSourceProviderMock()
        controller.dataSourceProvider = provider
        _ = controller.view
        controller.filterControl.selectedSegmentIndex = 3

        controller.filterAction(self)

        XCTAssertEqual(.all, provider.selectedFilter)
    }

    func testTraitCollectionDidChange_DarkModeEnabled_UpdatesTheDataSource() {
        let dataSource = AssetsTableDataSource()
        controller.dataSource = dataSource
        _ = controller.view

        controller.overrideUserInterfaceStyle = .dark

        XCTAssertEqual(dataSource.userInterfaceStyle, .dark)
    }

    func testTraitCollectionDidChange_LightModeEnabled_UpdatesTheDataSource() {
        let dataSource = AssetsTableDataSource()
        controller.dataSource = dataSource
        _ = controller.view

        controller.overrideUserInterfaceStyle = .light

        XCTAssertEqual(dataSource.userInterfaceStyle, .light)
    }
}
