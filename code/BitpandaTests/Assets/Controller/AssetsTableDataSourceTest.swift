//
//  AssetsTableDataSourceTest.swift
//  BitpandaTests
//
//  Created by Juan Andres Gebhard on 28/04/2022.
//

@testable import Bitpanda
import XCTest

class AssetsTableDataSourceTest: XCTestCase {
    var tableView: UITableView!

    override func setUpWithError() throws {
        let board = UIStoryboard(name: "Main", bundle: nil)
        let controller = (board.instantiateViewController(withIdentifier: "Assets") as! AssetsViewController)
        _ = controller.view
        tableView = controller.tableView
    }

    func testSetAssets_WithFiats_FiltersTheOnesWithoutWallets() throws {
        let dataSource = AssetsTableDataSource()
        let assets = try makeOneFiatWithWalletTwoWithout()

        dataSource.assets = assets

        XCTAssertEqual(1, dataSource.fiatDataSource.models.count)
        XCTAssertEqual(3, dataSource.assets.fiats.count)
    }

    func testNumberOfRows_FirstSection_ReturnsNumberOfCryptos() throws {
        let dataSource = AssetsTableDataSource()
        let assets = try make2Crypto3Comm1Fiat()

        dataSource.assets = assets

        XCTAssertEqual(assets.cryptocoins.count, dataSource.tableView(UITableView(), numberOfRowsInSection: 0))
    }

    func testNumberOfRows_SecondSection_ReturnsNumberOfCommodities() throws {
        let dataSource = AssetsTableDataSource()
        let assets = try make2Crypto3Comm1Fiat()

        dataSource.assets = assets

        XCTAssertEqual(assets.commodities.count, dataSource.tableView(UITableView(), numberOfRowsInSection: 1))
    }

    func testNumberOfRows_ThirdSection_ReturnsNumberOfFiat() throws {
        let dataSource = AssetsTableDataSource()
        let assets = try make2Crypto3Comm1Fiat()

        dataSource.assets = assets

        XCTAssertEqual(assets.fiats.count, dataSource.tableView(UITableView(), numberOfRowsInSection: 2))
    }

    func testNumberOfRows_FourthSection_ReturnsZero() throws {
        let dataSource = AssetsTableDataSource()
        let assets = try make2Crypto3Comm1Fiat()

        dataSource.assets = assets

        XCTAssertEqual(0, dataSource.tableView(UITableView(), numberOfRowsInSection: 3))
    }

    func testCell_CryptoSection_ReturnsAssetCellConfigured() throws {
        let dataSource = AssetsTableDataSource()
        let assets = try make2Crypto3Comm1Fiat()

        dataSource.assets = assets
        let cell = dataSource.tableView(tableView, cellForRowAt: IndexPath(row: 0, section: 0)) as! AssetTableViewCell

        XCTAssertEqual("Bitcoin", cell.nameLabel.text)
        XCTAssertEqual("8.936,50 €", cell.priceLabel.text)
        XCTAssertEqual("BTC", cell.symbolLabel.text)
    }

    func testCell_CommoditiesSection_ReturnsAssetCellConfigured() throws {
        let dataSource = AssetsTableDataSource()
        let assets = try make2Crypto3Comm1Fiat()

        dataSource.assets = assets
        let cell = dataSource.tableView(tableView, cellForRowAt: IndexPath(row: 0, section: 1)) as! AssetTableViewCell

        XCTAssertEqual("Gold", cell.nameLabel.text)
        XCTAssertEqual("46,20 €", cell.priceLabel.text)
        XCTAssertEqual("XAU", cell.symbolLabel.text)
    }

    func testCell_FiatsSection_ReturnsAssetCellConfigured() throws {
        let dataSource = AssetsTableDataSource()
        let assets = try make2Crypto3Comm1Fiat()

        dataSource.assets = assets
        let cell = dataSource.tableView(tableView, cellForRowAt: IndexPath(row: 0, section: 2)) as! AssetTableViewCell

        XCTAssertEqual("Euro", cell.nameLabel.text)
        XCTAssertEqual("", cell.priceLabel.text)
        XCTAssertEqual("EUR", cell.symbolLabel.text)
    }

    func testCell_InexistentSection_ReturnsUITableViewCell() throws {
        let dataSource = AssetsTableDataSource()
        let assets = try make2Crypto3Comm1Fiat()

        dataSource.assets = assets
        let cell = dataSource.tableView(tableView, cellForRowAt: IndexPath(row: 0, section: 3))

        XCTAssertEqual(type(of: cell).description(), "UITableViewCell")
    }

    func testFilteredDataSource_Crypto_ReturnsCryptoDataSource() {
        let dataSource = AssetsTableDataSource()

        let filtered = dataSource.filteredDataSource(filter: .cryptocoins)

        XCTAssert(filtered === dataSource.cryptoDataSource)
    }

    func testFilteredDataSource_Commodity_ReturnsCommodityDataSource() {
        let dataSource = AssetsTableDataSource()

        let filtered = dataSource.filteredDataSource(filter: .commodities)

        XCTAssert(filtered === dataSource.commoditiesDataSource)
    }

    func testFilteredDataSource_Fiat_ReturnsFiatDataSource() {
        let dataSource = AssetsTableDataSource()

        let filtered = dataSource.filteredDataSource(filter: .fiat)

        XCTAssert(filtered === dataSource.fiatDataSource)
    }

    func testFilteredDataSource_All_ReturnsSelf() {
        let dataSource = AssetsTableDataSource()

        let filtered = dataSource.filteredDataSource(filter: .all)

        XCTAssert(filtered === dataSource)
    }

    // MARK: Factory methods

    func makeOneFiatWithWalletTwoWithout() throws -> Assets {
        return try assetsFromFile("TestAssetsFiatsWithoutWallets")
    }

    func make2Crypto3Comm1Fiat() throws -> Assets {
        return try assetsFromFile("TestAssets2Crypto3Comm1Fiats")
    }

    func assetsFromFile(_ fileName: String) throws -> Assets {
        let parser = Parser<Assets>()
        let url = Bundle(for: type(of: self)).url(forResource: fileName, withExtension: "json")
        let data = try Data(contentsOf: url!)
        let result = parser.parse(data: data)
        if case let .success(assets) = result {
            return assets
        }
        throw ServiceError.resourceNotFound
    }
}
