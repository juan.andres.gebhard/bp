//
//  WalletsTableDataSourceTest.swift
//  BitpandaTests
//
//  Created by Juan Andres Gebhard on 29/04/2022.
//

@testable import Bitpanda
import XCTest

struct MockWallet: ListableWallet {
    var name: String

    var balance: NSNumber

    var symbol: String

    var icon: (id: String, assetType: WalletAssetType)

    var decoration: ListableWalletDecoration

    var deleted: Bool
}

class WalletsTableDataSourceTest: XCTestCase {
    var tableView: UITableView!
    var dataSource: WalletsTableDataSource!

    override func setUpWithError() throws {
        let board = UIStoryboard(name: "Main", bundle: nil)
        let controller = (board.instantiateViewController(withIdentifier: "Wallets") as! WalletsViewController)
        _ = controller.view
        tableView = controller.tableView
        dataSource = controller.dataSource
    }

    func testSetup_SetsWalletNameOnNameLabel() {
        let cell = makeCell()
        let wallet = makeDefaultWallet()

        dataSource.setup(cell, wallet, .light)

        XCTAssertEqual("Name", cell.nameLabel.text)
    }

    func testSetup_BalanceLabelUsesSymbolAndLocale() {
        let cell = makeCell()
        let wallet = makeDefaultWallet()

        dataSource.setup(cell, wallet, .light)

        XCTAssertEqual("33,00 €", cell.balanceLabel.text)
    }

    func makeDefaultWallet() -> ListableWallet {
        return MockWallet(name: "Name", balance: NSNumber(33),
                          symbol: "€", icon: ("iconId", .crypto),
                          decoration: .isDefault, deleted: false)
    }

    func makeCell() -> WalletTableViewCell {
        return (tableView.dequeueReusableCell(withIdentifier: dataSource.cellIdentifier) as! WalletTableViewCell)
    }
}
