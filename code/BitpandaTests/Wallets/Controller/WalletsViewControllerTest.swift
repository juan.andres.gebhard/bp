//
//  WalletsViewControllerTest.swift
//  BitpandaTests
//
//  Created by Juan Andres Gebhard on 29/04/2022.
//

@testable import Bitpanda
import XCTest

class WalletsViewControllerTest: XCTestCase {
    var controller: WalletsViewController!

    override func setUpWithError() throws {
        let board = UIStoryboard(name: "Main", bundle: nil)
        controller = (board.instantiateViewController(withIdentifier: "Wallets") as! WalletsViewController)
    }

    func testViewDidLoad_SetsDarkmodeToDataSource() {
        _ = controller.view

        XCTAssertEqual(controller.traitCollection.userInterfaceStyle, controller.dataSource.userInterfaceStyle)
    }

    func testTraitCollectionDidChange_FowardsToDataSource() {
        _ = controller.view

        controller.overrideUserInterfaceStyle = .light
        XCTAssertEqual(.light, controller.dataSource.userInterfaceStyle)
        controller.overrideUserInterfaceStyle = .dark
        XCTAssertEqual(.dark, controller.dataSource.userInterfaceStyle)
    }
}
