//
//  ListableWallet.swift
//  BitpandaTests
//
//  Created by Juan Andres Gebhard on 29/04/2022.
//

@testable import Bitpanda
import XCTest

class CryptoWalletListableWalletTest: XCTestCase {
    func testSymbol_ReturnCryptoSymbol() throws {
        let wallet: ListableWallet = try makeCryptoWallet()

        XCTAssertEqual("BTC", wallet.symbol)
    }

    func testDecoration_WithDefaultWallet_ReturnsDefault() throws {
        let wallet: ListableWallet = try makeCryptoWallet()

        XCTAssertEqual(.isDefault, wallet.decoration)
    }

    func testDecoration_WithNonDefaultWallet_ReturnsDefault() throws {
        let wallet: ListableWallet = try makeCryptoWalletThatIsNonDefault()

        XCTAssertEqual(.none, wallet.decoration)
    }

    func testIcon_withCryptoWallet_returnsCryptocoinId() throws {
        let cryptoWallet = try makeCryptoWallet()
        let wallet: ListableWallet = cryptoWallet

        XCTAssertEqual(.crypto, wallet.icon.assetType)
        XCTAssertEqual(cryptoWallet.cryptocoinId, wallet.icon.id)
    }

    func makeCryptoWallet() throws -> Wallet {
        let data = try TestMasterdata.data()
        return data.wallets.wallets.first!
    }

    func makeCryptoWalletThatIsNonDefault() throws -> Wallet {
        let data = try TestMasterdata.data()
        return data.wallets.wallets[1]
    }
}
