//
//  FiatWalletListableWalletTest.swift
//  BitpandaTests
//
//  Created by Juan Andres Gebhard on 29/04/2022.
//

@testable import Bitpanda
import XCTest

class FiatWalletListableWalletTest: XCTestCase {
    func testSymbol_ReturnFiatSymbol() throws {
        let wallet: ListableWallet = try makeFiatWallet()

        XCTAssertEqual("EUR", wallet.symbol)
    }

    func testDecoration_ReturnsFiat() throws {
        let wallet: ListableWallet = try makeFiatWallet()

        XCTAssertEqual(.isFiat, wallet.decoration)
    }

    func testIcon_withFiatWallet_returnsFiatId() throws {
        let fiatWallet = try makeFiatWallet()
        let wallet: ListableWallet = fiatWallet

        XCTAssertEqual(.fiat, wallet.icon.assetType)
        XCTAssertEqual(fiatWallet.fiatId, wallet.icon.id)
    }

    func testDeleted_ReturnsFalse() throws {
        let wallet: ListableWallet = try makeFiatWallet()

        XCTAssertFalse(wallet.deleted)
    }

    func makeFiatWallet() throws -> FiatWallet {
        let data = try TestMasterdata.data()
        return data.wallets.fiats.first!
    }
}
