//
//  WalletGroupTableViewCell.swift
//  Bitpanda
//
//  Created by Juan Andres Gebhard on 26/04/2022.
//

import UIKit

final class WalletGroupTableViewCell: UITableViewCell {
    @IBOutlet var iconImageView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var subtitleLabel: UILabel!
}
