//
//  WalletGroupsTableDataSource.swift
//  Bitpanda
//
//  Created by Juan Andres Gebhard on 26/04/2022.
//

import Foundation
import UIKit

final class WalletGroupsTableDataSourceFactory {
    func dataSource(wallets: Wallets) -> ArrayDataSource<WalletGroup, WalletGroupTableViewCell> {
        let groups = createGroups(from: wallets)
        let dataSource = ArrayDataSource<WalletGroup, WalletGroupTableViewCell>(cellIdentifier: "Cell") { cell, group, _ in
            cell.titleLabel.text = group.name
            cell.subtitleLabel.text = "\(group.wallets.count) wallets"
            cell.iconImageView.image = UIImage(systemName: group.icon)
        }
        dataSource.models = groups
        return dataSource
    }

    private func createGroups(from wallets: Wallets) -> [WalletGroup] {
        var groups = [WalletGroup]()
        groups.append(WalletGroup(name: "Cryptocoins",
                                  icon: "bitcoinsign.circle",
                                  wallets: wallets.wallets))
        groups.append(WalletGroup(name: "Commodities",
                                  icon: "diamond.circle",
                                  wallets: wallets.commodities))
        groups.append(WalletGroup(name: "Fiat",
                                  icon: "dollarsign.circle",
                                  wallets: wallets.fiats))
        return groups
    }
}
