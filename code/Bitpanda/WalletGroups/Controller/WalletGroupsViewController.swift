//
//  WalletSelectorViewController.swift
//  Bitpanda
//
//  Created by Juan Andres Gebhard on 26/04/2022.
//

import UIKit

final class WalletGroupsViewController: UIViewController, HasMasterData {
    @IBOutlet var tableView: UITableView?

    @IBOutlet var loadingController: OverlayController!

    @IBOutlet var errorController: OverlayController!

    var state: State<MasterData> = State.loading {
        didSet {
            updateState()
        }
    }

    var dataSource: ArrayDataSource<WalletGroup, WalletGroupTableViewCell>?

    override func viewDidLoad() {
        super.viewDidLoad()
        updateState()
    }

    func showLoading() {
        errorController.hide()
        loadingController.show()
    }

    func show(wallets: Wallets) {
        loadingController.hide()
        dataSource = WalletGroupsTableDataSourceFactory().dataSource(wallets: wallets)
        tableView?.dataSource = dataSource
        tableView?.reloadData()
    }

    func show(error _: Error, retryBlock: @escaping () -> Void) {
        loadingController.hide()
        errorController.show()
        errorController.callback = retryBlock
    }

    func updateState() {
        switch state {
        case .loading:
            showLoading()
        case let .data(data):
            show(wallets: data.wallets)
        case let .error(error, callback):
            show(error: error, retryBlock: callback)
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender _: Any?) {
        guard let selectedRow = tableView?.indexPathForSelectedRow?.row,
              let wallets = dataSource?.models[selectedRow].wallets,
              let title = dataSource?.models[selectedRow].name,
              case let .data(data) = state,
              let destination = segue.destination as? WalletsViewController
        else {
            return
        }
        destination.wallets = wallets
        destination.assets = data.assets
        destination.navigationItem.title = title
    }
}
