//
//  WalletGroup.swift
//  Bitpanda
//
//  Created by Juan Andres Gebhard on 30/04/2022.
//

import Foundation

struct WalletGroup {
    let name: String
    let icon: String
    let wallets: [ListableWallet]
}
