//
//  TabBarBroadcaster.swift
//  Bitpanda
//
//  Created by Juan Andres Gebhard on 26/04/2022.
//

import Foundation
import UIKit

protocol HasMasterData {
    var state: State<MasterData> { get set }
}

extension UITabBarController {
    func broadcast(_ data: State<MasterData>) {
        guard let viewControllers = viewControllers else {
            return
        }
        for viewController in viewControllers {
            if let navigationController = viewController as? UINavigationController, var firstChild = navigationController.viewControllers.first as? HasMasterData {
                firstChild.state = data
            } else if var viewController = viewController as? HasMasterData {
                viewController.state = data
            }
        }
    }
}
