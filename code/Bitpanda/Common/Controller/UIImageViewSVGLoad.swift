//
//  UIImageViewSVGLoad.swift
//  Bitpanda
//
//  Created by Juan Andres Gebhard on 24/04/2022.
//

import Foundation
import SVGKit
import UIKit

extension UIImageView {
    func loadSVG(url: URL, downloader: Downloader, success: @escaping () -> Void = {}) {
        downloader.download(url: url) { [weak self] (result: Result<Data, Error>) in
            switch result {
            case let .success(data):
                let svg = SVGKImage(data: data)
                self?.image = svg?.uiImage
                success()
            case let .failure(error):
                print(error)
            }
        }
    }
}
