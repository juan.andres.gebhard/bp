//
//  LoadingController.swift
//  Bitpanda
//
//  Created by Juan Andres Gebhard on 26/04/2022.
//

import Foundation
import UIKit

final class OverlayController: NSObject {
    let animationDuration: TimeInterval = 0.30

    var overlayView: UIView?

    @IBOutlet var containerView: UIView? {
        didSet {
            if showAtFirst { show() }
        }
    }

    @IBInspectable var nibName: String!

    @IBInspectable var showAtFirst: Bool = false

    var callback: () -> Void = {}

    @IBAction
    func userAction() {
        callback()
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        let nib = Bundle.main.loadNibNamed(nibName, owner: self, options: nil)
        if let view = nib?.first as? UIView {
            overlayView = view
        }
    }

    func show() {
        overlayView?.removeFromSuperview()
        overlayView?.alpha = 0
        if let view = overlayView {
            containerView?.placeSubviewAlongTheEdges(view)
        }
        UIView.animate(withDuration: animationDuration) {
            self.overlayView?.alpha = 1
        }
    }

    func hide() {
        UIView.animate(withDuration: animationDuration) {
            self.overlayView?.alpha = 0
        } completion: { _ in
            self.overlayView?.removeFromSuperview()
        }
    }
}
