//
//  PriceFormatter.swift
//  Bitpanda
//
//  Created by Juan Andres Gebhard on 24/04/2022.
//

import Foundation

extension NumberFormatter {
    class func priceFormatter(precision: Int) -> NumberFormatter {
        let currencyFormatter = self.init()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = .currency
        currencyFormatter.locale = Locale.current
        currencyFormatter.maximumFractionDigits = precision
        currencyFormatter.minimumFractionDigits = precision
        return currencyFormatter
    }
}
