//
//  ArrayDataSource.swift
//  Bitpanda
//
//  Created by Juan Andres Gebhard on 23/04/2022.
//

import Foundation
import UIKit

class ArrayDataSource<Model, View: UITableViewCell>: NSObject, UITableViewDataSource {
    final let errorCellId = "Error"

    final var cellIdentifier: String = ""

    final var models: [Model] = []

    final var setup: (View, Model, UIUserInterfaceStyle) -> Void = { _, _, _ in }

    final var userInterfaceStyle: UIUserInterfaceStyle = .light

    @objc final func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return models.count
    }

    @objc final func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? View else {
            return UITableViewCell()
        }
        setup(cell, models[indexPath.row], userInterfaceStyle)
        return cell
    }

    init(cellIdentifier: String, setup: @escaping (View, Model, UIUserInterfaceStyle) -> Void = { _, _, _ in }) {
        self.cellIdentifier = cellIdentifier
        self.setup = setup
    }
}
