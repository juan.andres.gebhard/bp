//
//  AttributesContainer.swift
//  Bitpanda
//
//  Created by Juan Andres Gebhard on 23/04/2022.
//

import Foundation

enum AttributesContainer: String, CodingKey {
    case id
    case attributes
}
