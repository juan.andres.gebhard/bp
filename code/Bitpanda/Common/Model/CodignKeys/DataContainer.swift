//
//  DataContainer.swift
//  Bitpanda
//
//  Created by Juan Andres Gebhard on 26/04/2022.
//

import Foundation

enum DataContainer: String, CodingKey {
    case data
}
