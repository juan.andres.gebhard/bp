//
//  State.swift
//  Bitpanda
//
//  Created by Juan Andres Gebhard on 26/04/2022.
//

import Foundation

/// Represents the state of an element
enum State<DataType: Equatable>: Equatable {
    case loading
    case data(DataType)
    case error(Error, () -> Void)

    static func == (lhs: State<DataType>, rhs: State<DataType>) -> Bool {
        switch (lhs, rhs) {
        case (.loading, .loading):
            return true
        case let (.data(lData), .data(rData)):
            return lData == rData
        case let (.error(lError, _), .error(rError, _)):
            return type(of: lError) == type(of: rError)
        default:
            return false
        }
    }
}
