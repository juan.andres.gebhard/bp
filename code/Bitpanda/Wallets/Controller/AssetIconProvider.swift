//
//  AssetIconProvider.swift
//  Bitpanda
//
//  Created by Juan Andres Gebhard on 26/04/2022.
//

import Foundation
import UIKit

/**
 Provider to help find the icon URL for a given asset Id and type.
 */
struct AssetIconProvider {
    let assets: Assets
    let style: UIUserInterfaceStyle

    private func cryptoLogoURL(_ id: String) -> URL? {
        let cryto = assets.cryptocoins.first { $0.id == id }
        let commodity = assets.commodities.first { $0.id == id }
        if let asset = cryto ?? commodity {
            let urlString = style == .dark ? asset.logoDark : asset.logo
            return URL(string: urlString)
        }
        return nil
    }

    private func fiatLogoUrl(_ id: String) -> URL? {
        if let fiat = assets.fiats.first(where: { $0.id == id }) {
            let urlString = style == .dark ? fiat.logoDark : fiat.logo
            return URL(string: urlString)
        }
        return nil
    }

    func logoUrl(forType type: WalletAssetType, id: String) -> URL? {
        switch type {
        case .crypto:
            return cryptoLogoURL(id)
        case .fiat:
            return fiatLogoUrl(id)
        }
    }
}
