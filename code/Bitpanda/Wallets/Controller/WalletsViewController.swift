//
//  WalletsViewController.swift
//  Bitpanda
//
//  Created by Juan Andres Gebhard on 26/04/2022.
//

import UIKit

final class WalletsViewController: UIViewController {
    var wallets: [ListableWallet] = [] {
        didSet {
            let listToDisplay = wallets.sorted { $0.balance.doubleValue > $1.balance.doubleValue }
                .filter { !$0.deleted }
            dataSource.models = listToDisplay
            tableView?.reloadData()
        }
    }

    var dataSource = WalletsTableDataSource()

    var assets: Assets = .init() {
        didSet {
            dataSource.assets = assets
        }
    }

    @IBOutlet var tableView: UITableView?

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView?.dataSource = dataSource
        dataSource.userInterfaceStyle = traitCollection.userInterfaceStyle
    }

    override func traitCollectionDidChange(_: UITraitCollection?) {
        dataSource.userInterfaceStyle = traitCollection.userInterfaceStyle
        tableView?.reloadData()
    }
}
