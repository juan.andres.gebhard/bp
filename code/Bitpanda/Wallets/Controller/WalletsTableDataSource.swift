//
//  WalletsTableDataSource.swift
//  Bitpanda
//
//  Created by Juan Andres Gebhard on 26/04/2022.
//

import Foundation
import UIKit

final class WalletsTableDataSource: ArrayDataSource<ListableWallet, WalletTableViewCell> {
    var assets: Assets = .init()

    init() {
        super.init(cellIdentifier: "Cell")
        setup = { cell, wallet, style in
            cell.nameLabel.text = wallet.name
            cell.balanceLabel.text = self.balanceString(balance: wallet.balance, symbol: wallet.symbol)
            self.decorate(cell.accessoryImageView, with: wallet.decoration)
            self.fetchIcon(assetId: wallet.icon.id,
                           type: wallet.icon.assetType,
                           cell: cell,
                           style: style)
        }
    }

    private func balanceString(balance: NSNumber, symbol: String) -> String? {
        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = .currency
        currencyFormatter.locale = Locale.current
        currencyFormatter.currencySymbol = symbol
        return currencyFormatter.string(from: balance)
    }

    private func decorate(_ imageView: UIImageView, with decoration: ListableWalletDecoration) {
        imageView.isHidden = false
        switch decoration {
        case .isFiat:
            imageView.image = UIImage(systemName: "dollarsign.circle.fill")
        case .isDefault:
            imageView.image = UIImage(systemName: "star.fill")
        case .none:
            imageView.isHidden = true
        }
    }

    private func fetchIcon(assetId: String, type: WalletAssetType, cell: WalletTableViewCell,
                           style: UIUserInterfaceStyle)
    {
        cell.downloader?.cancel()
        let provider = AssetIconProvider(assets: assets, style: style)
        let logoUrl = provider.logoUrl(forType: type, id: assetId)
        cell.downloader = CachedDownloader()
        if let logoUrl = logoUrl, let downloader = cell.downloader {
            cell.iconImageView.loadSVG(url: logoUrl, downloader: downloader)
        }
    }
}
