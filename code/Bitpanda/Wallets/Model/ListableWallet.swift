//
//  ListableWallet.swift
//  Bitpanda
//
//  Created by Juan Andres Gebhard on 26/04/2022.
//

import Foundation

enum ListableWalletDecoration {
    case isDefault
    case isFiat
    case none
}

enum WalletAssetType {
    case crypto
    case fiat
}

protocol ListableWallet {
    var name: String { get }
    var balance: NSNumber { get }
    var symbol: String { get }
    var icon: (id: String, assetType: WalletAssetType) { get }
    var decoration: ListableWalletDecoration { get }
    var deleted: Bool { get }
}

extension Wallet: ListableWallet {
    var symbol: String {
        return cryptocoinSymbol
    }

    var decoration: ListableWalletDecoration {
        isDefault ? .isDefault : .none
    }

    var icon: (id: String, assetType: WalletAssetType) {
        return (cryptocoinId, .crypto)
    }
}

extension FiatWallet: ListableWallet {
    var symbol: String {
        return fiatSymbol
    }

    var deleted: Bool { return false }

    var decoration: ListableWalletDecoration {
        return .isFiat
    }

    var icon: (id: String, assetType: WalletAssetType) {
        return (fiatId, .fiat)
    }
}
