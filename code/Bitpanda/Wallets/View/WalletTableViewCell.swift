//
//  WalletTableViewCell.swift
//  Bitpanda
//
//  Created by Juan Andres Gebhard on 26/04/2022.
//

import UIKit

final class WalletTableViewCell: UITableViewCell {
    @IBOutlet var cardView: UIView!

    @IBOutlet var nameLabel: UILabel!

    @IBOutlet var balanceLabel: UILabel!

    @IBOutlet var accessoryImageView: UIImageView!

    @IBOutlet var iconImageView: UIImageView!

    var downloader: Downloader?
}
