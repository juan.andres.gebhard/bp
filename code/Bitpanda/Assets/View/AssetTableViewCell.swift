//
//  CryptocoinTableViewCell.swift
//  Bitpanda
//
//  Created by Juan Andres Gebhard on 23/04/2022.
//

import SVGKit
import UIKit

final class AssetTableViewCell: UITableViewCell {
    @IBOutlet var iconImageView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var symbolLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    var downloader: Downloader?

    override func prepareForReuse() {
        super.prepareForReuse()
        iconImageView.image = nil
        activityIndicator.startAnimating()
    }
}
