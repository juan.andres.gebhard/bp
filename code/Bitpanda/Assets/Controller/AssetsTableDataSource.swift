//
//  AssetsTableDataSource.swift
//  Bitpanda
//
//  Created by Juan Andres Gebhard on 23/04/2022.
//

import Foundation
import UIKit

final class AssetsTableDataSource: NSObject, UITableViewDataSource, FilteredDataSourceProvider {
    var cryptoDataSource = DataSourceFactory.cryptoDataSource()
    var commoditiesDataSource = DataSourceFactory.commoditiesDataSource()
    var fiatDataSource = DataSourceFactory.fiatDataSource()

    var userInterfaceStyle: UIUserInterfaceStyle = .light {
        didSet {
            cryptoDataSource.userInterfaceStyle = userInterfaceStyle
            commoditiesDataSource.userInterfaceStyle = userInterfaceStyle
            fiatDataSource.userInterfaceStyle = userInterfaceStyle
        }
    }

    var assets = Assets() {
        didSet {
            cryptoDataSource.models = assets.cryptocoins
            commoditiesDataSource.models = assets.commodities
            fiatDataSource.models = assets.fiats.filter { $0.hasWallets }
        }
    }

    func numberOfSections(in _: UITableView) -> Int {
        return AssetType.all.rawValue
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionEnum = AssetType(rawValue: section)
        switch sectionEnum {
        case .cryptocoins:
            return cryptoDataSource.tableView(tableView, numberOfRowsInSection: section)
        case .commodities:
            return commoditiesDataSource.tableView(tableView, numberOfRowsInSection: section)
        case .fiat:
            return fiatDataSource.tableView(tableView, numberOfRowsInSection: section)
        case .all, .none:
            return 0
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sectionEnum = AssetType(rawValue: indexPath.section)
        switch sectionEnum {
        case .cryptocoins:
            return cryptoDataSource.tableView(tableView, cellForRowAt: indexPath)
        case .commodities:
            return commoditiesDataSource.tableView(tableView, cellForRowAt: indexPath)
        case .fiat:
            return fiatDataSource.tableView(tableView, cellForRowAt: indexPath)
        case .all, .none:
            return UITableViewCell()
        }
    }

    func filteredDataSource(filter: AssetType) -> UITableViewDataSource {
        switch filter {
        case .commodities: return commoditiesDataSource
        case .cryptocoins: return cryptoDataSource
        case .fiat: return fiatDataSource
        case .all: return self
        }
    }
}
