//
//  DataSourceFactory.swift
//  Bitpanda
//
//  Created by Juan Andres Gebhard on 23/04/2022.
//

import Foundation
import SVGKit

final class DataSourceFactory {
    private static let cellId = "AssetCell"

    class func cryptoDataSource() -> ArrayDataSource<Cryptocoin, AssetTableViewCell> {
        return ArrayDataSource<Cryptocoin, AssetTableViewCell>(cellIdentifier: cellId) { cell, coin, style in
            cell.nameLabel.text = coin.name
            cell.symbolLabel.text = coin.symbol
            let formatter = NumberFormatter.priceFormatter(precision: coin.precisionForFiatPrice)
            cell.priceLabel.text = formatter.string(from: coin.avgPrice)
            let logoURL = style == .dark ? coin.logoDark : coin.logo
            loadLogo(for: cell, url: logoURL)
        }
    }

    class func commoditiesDataSource() -> ArrayDataSource<Commodity, AssetTableViewCell> {
        return cryptoDataSource()
    }

    class func fiatDataSource() -> ArrayDataSource<Fiat, AssetTableViewCell> {
        ArrayDataSource<Fiat, AssetTableViewCell>(cellIdentifier: cellId) { cell, fiat, style in
            cell.nameLabel.text = fiat.name
            cell.symbolLabel.text = fiat.symbol
            cell.priceLabel.text = ""
            let logoURL = style == .dark ? fiat.logoDark : fiat.logo
            loadLogo(for: cell, url: logoURL)
        }
    }

    class func loadLogo(for cell: AssetTableViewCell, url: String) {
        cell.downloader?.cancel()
        if let url = URL(string: url) {
            let downloader = CachedDownloader()
            cell.iconImageView.loadSVG(url: url, downloader: downloader) {
                cell.activityIndicator.stopAnimating()
            }
            cell.downloader = downloader
        }
    }
}
