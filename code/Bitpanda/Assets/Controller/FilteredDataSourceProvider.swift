//
//  FilteredDataSourceProvider.swift
//  Bitpanda
//
//  Created by Juan Andres Gebhard on 28/04/2022.
//

import Foundation
import UIKit

@objc enum AssetType: Int {
    case cryptocoins = 0
    case commodities = 1
    case fiat = 2
    case all = 3
}

@objc protocol FilteredDataSourceProvider {
    func filteredDataSource(filter: AssetType) -> UITableViewDataSource
}
