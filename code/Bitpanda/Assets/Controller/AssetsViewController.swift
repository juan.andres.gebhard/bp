//
//  AssetsViewController.swift
//  Bitpanda
//
//  Created by Juan Andres Gebhard on 23/04/2022.
//

import UIKit

final class AssetsViewController: UIViewController {
    let animationDuration: TimeInterval = 0.35

    @IBOutlet var filterControl: UISegmentedControl!
    @IBOutlet var dataSource: AssetsTableDataSource!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var loadingController: OverlayController!
    @IBOutlet var errorController: OverlayController!

    @IBOutlet var dataSourceProvider: FilteredDataSourceProvider!

    var service: MasterDataService = LocalFileService()

    private(set) var state: State<MasterData> = .loading {
        didSet {
            self.tabBarController?.broadcast(state)
            switch state {
            case .loading: showLoading()
            case let .data(data): showData(data)
            case let .error(error, callback): showError(error, callback: callback)
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        loadAssets()
        dataSource.userInterfaceStyle = traitCollection.userInterfaceStyle
    }

    private func loadAssets() {
        state = .loading
        service.fetchAssets { (result: Result<MasterData, Error>) in
            switch result {
            case let .success(data): self.state = .data(data)
            case let .failure(error): self.state = .error(error) { self.loadAssets() }
            }
        }
    }

    @IBAction func filterAction(_: Any) {
        let index = filterControl.selectedSegmentIndex
        if let selectedType = AssetType(rawValue: index) {
            let filteredDataSource = dataSourceProvider.filteredDataSource(filter: selectedType)
            tableView.dataSource = filteredDataSource
            UIView.transition(with: tableView,
                              duration: animationDuration,
                              options: .transitionCrossDissolve,
                              animations: { self.tableView.reloadData() })
        }
    }

    private func showData(_ data: MasterData) {
        loadingController.hide()
        dataSource.assets = data.assets
        tableView.reloadData()
    }

    private func showLoading() {
        errorController.hide()
        loadingController.show()
    }

    private func showError(_: Error, callback: @escaping () -> Void) {
        errorController.callback = callback
        errorController.show()
    }

    override func traitCollectionDidChange(_: UITraitCollection?) {
        dataSource.userInterfaceStyle = traitCollection.userInterfaceStyle
        tableView.reloadData()
    }
}
