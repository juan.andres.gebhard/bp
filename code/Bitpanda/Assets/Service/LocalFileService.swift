//
//  LocalFileService.swift
//  Bitpanda
//
//  Created by Juan Andres Gebhard on 23/04/2022.
//

import Foundation

final class LocalFileService: MasterDataService {
    var first = true
    var delay: TimeInterval = 2

    func fetchAssets(completion: @escaping (Result<MasterData, Error>) -> Void) {
        let file = first ? "wrongName" : "Masterdata"
        first = false
        DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
            self.fetchFromFile(fileName: file, completion: completion)
        }
    }

    func fetchFromFile<DataType: Decodable>(fileName: String,
                                            completion: @escaping (Result<DataType, Error>) -> Void)
    {
        let url = Bundle.main.url(forResource: fileName, withExtension: "json")
        guard let url = url else {
            completion(Result.failure(ServiceError.resourceNotFound))
            return
        }
        do {
            let data = try Data(contentsOf: url)
            completion(Parser<DataType>().parse(data: data))
        } catch {
            completion(Result.failure(error))
        }
    }
}
