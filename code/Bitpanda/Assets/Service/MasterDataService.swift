//
//  AssetService.swift
//  Bitpanda
//
//  Created by Juan Andres Gebhard on 23/04/2022.
//

import Foundation

enum ServiceError: Error {
    case resourceNotFound
}

protocol MasterDataService {
    func fetchAssets(completion: @escaping (Result<MasterData, Error>) -> Void)
}
