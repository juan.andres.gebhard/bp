//
//  DefaultImageDownloader.swift
//  Bitpanda
//
//  Created by Juan Andres Gebhard on 24/04/2022.
//

import Foundation

/**
 Downloads data from an URL only if there is nothing cached.
 */
final class CachedDownloader: Downloader {
    var session: URLSession
    private var task: URLSessionDataTask?

    init() {
        let configuration = URLSessionConfiguration.default
        configuration.requestCachePolicy = .returnCacheDataElseLoad
        session = URLSession(configuration: configuration)
    }

    func download(url: URL, completion: @escaping (Result<Data, Error>) -> Void) {
        task = session.dataTask(with: url) { (data: Data?, _: URLResponse?, error: Error?) in
            if let data = data {
                DispatchQueue.main.sync { completion(.success(data)) }
            }
            if let error = error {
                DispatchQueue.main.sync { completion(.failure(error)) }
            }
        }
        task?.resume()
    }

    func cancel() {
        task?.cancel()
    }
}
