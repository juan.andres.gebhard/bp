//
//  ImageDownloader.swift
//  Bitpanda
//
//  Created by Juan Andres Gebhard on 24/04/2022.
//

import Foundation
import UIKit

protocol Downloader {
    func download(url: URL, completion: @escaping (Result<Data, Error>) -> Void)
    func cancel()
}
