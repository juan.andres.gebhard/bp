//
//  FiatWallet.swift
//  Bitpanda
//
//  Created by Juan Andres Gebhard on 26/04/2022.
//

import Foundation

struct FiatWallet: Decodable, Equatable {
    let fiatId: String
    let balance: NSNumber
    let name: String
    let fiatSymbol: String

    enum WalletKeys: String, CodingKey {
        case fiatId
        case balance
        case name
        case fiatSymbol
    }

    init(from decoder: Decoder) throws {
        let mainContainer = try decoder.container(keyedBy: AttributesContainer.self)
        let attributes = try mainContainer.nestedContainer(keyedBy: WalletKeys.self, forKey: .attributes)
        fiatId = try attributes.decode(String.self, forKey: .fiatId)
        name = try attributes.decode(String.self, forKey: .name)
        fiatSymbol = try attributes.decode(String.self, forKey: .fiatSymbol)
        balance = try FiatWallet.balance(from: attributes)
    }

    private static func balance(from attributes: KeyedDecodingContainer<WalletKeys>) throws -> NSNumber {
        let balanceString = try attributes.decode(String.self, forKey: .balance)
        if let balanceDouble = Double(balanceString) {
            return NSNumber(value: balanceDouble)
        } else {
            return NSNumber(value: 0)
        }
    }
}
