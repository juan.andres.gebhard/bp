//
//  Wallets.swift
//  Bitpanda
//
//  Created by Juan Andres Gebhard on 26/04/2022.
//

import Foundation

struct Wallets: Decodable, Equatable {
    let wallets: [Wallet]
    let commodities: [Wallet]
    let fiats: [FiatWallet]

    enum WalletsContainer: String, CodingKey {
        case wallets
        case commodityWallets
        case fiatwallets
    }

    init(from decoder: Decoder) throws {
        let data = try decoder.container(keyedBy: WalletsContainer.self)

        wallets = try data.decode([Wallet].self, forKey: .wallets)
        commodities = try data.decode([Wallet].self, forKey: .commodityWallets)
        fiats = try data.decode([FiatWallet].self, forKey: .fiatwallets)
    }

    init() {
        wallets = []
        commodities = []
        fiats = []
    }

    static func == (lhs: Wallets, _: Wallets) -> Bool {
        return (lhs.wallets == lhs.wallets)
            && (lhs.commodities == lhs.commodities)
    }
}
