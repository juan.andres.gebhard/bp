//
//  Wallet.swift
//  Bitpanda
//
//  Created by Juan Andres Gebhard on 26/04/2022.
//

import Foundation

struct Wallet: Decodable, Equatable {
    let cryptocoinId: String
    let balance: NSNumber
    let name: String
    let cryptocoinSymbol: String
    let isDefault: Bool
    let deleted: Bool

    enum WalletKeys: String, CodingKey {
        case cryptocoinId
        case balance
        case name
        case cryptocoinSymbol
        case isDefault
        case deleted
    }

    init(from decoder: Decoder) throws {
        let mainContainer = try decoder.container(keyedBy: AttributesContainer.self)
        let attributes = try mainContainer.nestedContainer(keyedBy: WalletKeys.self, forKey: .attributes)
        cryptocoinId = try attributes.decode(String.self, forKey: .cryptocoinId)
        name = try attributes.decode(String.self, forKey: .name)
        cryptocoinSymbol = try attributes.decode(String.self, forKey: .cryptocoinSymbol)
        isDefault = try attributes.decode(Bool.self, forKey: .isDefault)
        deleted = try attributes.decode(Bool.self, forKey: .deleted)
        balance = try Wallet.balance(from: attributes)
    }

    private static func balance(from attributes: KeyedDecodingContainer<WalletKeys>) throws -> NSNumber {
        let balanceString = try attributes.decode(String.self, forKey: .balance)
        if let balanceDouble = Double(balanceString) {
            return NSNumber(value: balanceDouble)
        } else {
            return NSNumber(value: 0)
        }
    }
}
