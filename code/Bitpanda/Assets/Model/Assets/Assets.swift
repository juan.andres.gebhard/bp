//
//  Assets.swift
//  Bitpanda
//
//  Created by Juan Andres Gebhard on 23/04/2022.
//

import Foundation

struct Assets: Decodable, Equatable {
    static func == (lhs: Assets, rhs: Assets) -> Bool {
        return lhs.commodities == rhs.commodities
            && lhs.cryptocoins == rhs.cryptocoins
            && lhs.fiats == rhs.fiats
    }

    let cryptocoins: [Cryptocoin]
    let commodities: [Commodity]
    let fiats: [Fiat]

    enum AttributesContainer: String, CodingKey {
        case attributes
    }

    enum AssetsContainer: String, CodingKey {
        case cryptocoins
        case commodities
        case fiats
    }

    init() {
        commodities = []
        cryptocoins = []
        fiats = []
    }
}
