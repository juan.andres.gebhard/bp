//
//  Fiat.swift
//  Bitpanda
//
//  Created by Juan Andres Gebhard on 23/04/2022.
//

import Foundation

struct Fiat: Decodable, Equatable {
    let id: String
    let name: String
    let symbol: String
    let logo: String
    let logoDark: String
    let hasWallets: Bool

    enum FiatKeys: String, CodingKey {
        case name
        case symbol
        case logo
        case logoDark
        case hasWallets
    }

    init(from decoder: Decoder) throws {
        let mainContainer = try decoder.container(keyedBy: AttributesContainer.self)
        id = try mainContainer.decode(String.self, forKey: .id)
        let attributes = try mainContainer.nestedContainer(keyedBy: FiatKeys.self, forKey: .attributes)
        name = try attributes.decode(String.self, forKey: .name)
        symbol = try attributes.decode(String.self, forKey: .symbol)
        logo = try attributes.decode(String.self, forKey: .logo)
        logoDark = try attributes.decode(String.self, forKey: .logoDark)
        hasWallets = try attributes.decode(Bool.self, forKey: .hasWallets)
    }
}
