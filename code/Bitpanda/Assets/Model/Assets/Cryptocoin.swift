//
//  Cryptocoin.swift
//  Bitpanda
//
//  Created by Juan Andres Gebhard on 23/04/2022.
//

import Foundation

typealias Commodity = Cryptocoin

struct Cryptocoin: Decodable, Equatable {
    let id: String
    let name: String
    let symbol: String
    let avgPrice: NSNumber
    let logo: String
    let logoDark: String
    let precisionForFiatPrice: Int

    enum CryptoKeys: String, CodingKey {
        case name
        case symbol
        case avgPrice
        case logo
        case logoDark
        case precisionForFiatPrice
    }

    init(from decoder: Decoder) throws {
        let mainContainer = try decoder.container(keyedBy: AttributesContainer.self)
        id = try mainContainer.decode(String.self, forKey: .id)
        let attributes = try mainContainer.nestedContainer(keyedBy: CryptoKeys.self, forKey: .attributes)
        name = try attributes.decode(String.self, forKey: .name)
        symbol = try attributes.decode(String.self, forKey: .symbol)
        avgPrice = try Cryptocoin.price(from: attributes)
        logo = try attributes.decode(String.self, forKey: .logo)
        logoDark = try attributes.decode(String.self, forKey: .logoDark)
        precisionForFiatPrice = try attributes.decode(Int.self, forKey: .precisionForFiatPrice)
    }

    private static func price(from attributes: KeyedDecodingContainer<CryptoKeys>) throws -> NSNumber {
        let avgPriceString = try attributes.decode(String.self, forKey: .avgPrice)
        guard let price = Double(avgPriceString) else {
            let context = DecodingError.Context(codingPath: [CryptoKeys.avgPrice],
                                                debugDescription: "Unable to convert price to double",
                                                underlyingError: nil)
            throw DecodingError.valueNotFound(Double.self, context)
        }
        return NSNumber(value: price)
    }
}
