//
//  MasterData.swift
//  Bitpanda
//
//  Created by Juan Andres Gebhard on 27/04/2022.
//

import Foundation

struct MasterData: Decodable, Equatable {
    let assets: Assets
    let wallets: Wallets

    enum AttributesContainer: String, CodingKey {
        case attributes
    }

    init(from decoder: Decoder) throws {
        let data = try decoder.container(keyedBy: DataContainer.self)
        let dataContainer = try data.nestedContainer(keyedBy: AttributesContainer.self,
                                                     forKey: .data)

        assets = try dataContainer.decode(Assets.self, forKey: .attributes)
        wallets = try dataContainer.decode(Wallets.self, forKey: .attributes)
    }

    init() {
        assets = Assets()
        wallets = Wallets()
    }
}
